﻿// <copyright file="TrackerTests.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NUnit.Framework;

    /// <summary>
    /// Tests the features of the Tracker class.
    /// </summary>
    [TestFixture]
    public class TrackerTests
    {
        private Tracker testTracker;

        /// <summary>
        /// Test if the constructor runs.
        /// </summary>
        [Test]
        [Order(1)]
        public void CreateTest()
        {
            this.testTracker = new Tracker(@"HTTP:\\DISTRIBUTEDARCHIVE.LOCAL\tracker\");
        }

        /// <summary>
        /// Check if the input url was properly processed.
        /// </summary>
        [Test]
        [Order(2)]
        public void CtorUrlProcessingTest()
        {
            Assert.That(this.testTracker.Url, Is.EqualTo("http://distributedarchive.local/tracker"));
        }

        /// <summary>
        /// Check if the repository can find and read the tracker by url.
        /// </summary>
        [Test]
        [Order(2)]
        public void FindByUrlTest()
        {
            Assert.That(Tracker.FindByUrl("http://distributedarchive.local/tracker"), Is.Not.Null);
        }

        /// <summary>
        /// Check if the url based lookup works with different formatting.
        /// </summary>
        [Test]
        [Order(2)]
        public void FindByUrlAutoProcessingTest()
        {
            Assert.That(Tracker.FindByUrl(@"HTTP:\\DISTRIBUTEDARCHIVE.LOCAL\tracker\"), Is.Not.Null);
        }

        /// <summary>
        /// Check if the read operation completes.
        /// </summary>
        [Test]
        [Order(2)]
        public void ReadTest()
        {
            Assert.That(this.testTracker.Reload(), Is.True);
        }

        /// <summary>
        /// Check if the update operation completes.
        /// </summary>
        [Test]
        [Order(2)]
        public void UpdateTest()
        {
            Assert.That(this.testTracker.Save(), Is.True);
        }

        /// <summary>
        /// Check if the delete operation completes.
        /// </summary>
        [Test]
        [Order(3)]
        public void DeleteTest()
        {
            Assert.That(this.testTracker.Delete(), Is.True);
        }

        /// <summary>
        /// Check if the read operation fails after delete.
        /// </summary>
        [Test]
        [Order(4)]
        public void ReadFailTest()
        {
            Assert.That(this.testTracker.Reload(), Is.False);
        }

        /// <summary>
        /// Check if the update operation fails after delete.
        /// </summary>
        [Test]
        [Order(4)]
        public void UpdateFailTest()
        {
            Assert.That(this.testTracker.Save(), Is.False);
        }
    }
}

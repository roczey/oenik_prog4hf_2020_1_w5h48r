﻿// <copyright file="Tracker.LocalDbRepository.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// LocalDbRepository wrapper partial class.
    /// </summary>
    public partial class Tracker
    {
        /// <summary>
        /// Tracker repository using the EntityFramework LocalDb.
        /// </summary>
        public sealed class LocalDbRepository : AbstractRepository
        {
            private static readonly Data.LocalDbEntities DbSingleton = new Data.LocalDbEntities();
            private static readonly object IdCounterMutex = new object();
            private static int idCounter = DbSingleton.trackers.Max(x => x.id);

            /// <inheritdoc/>
            public override Tracker FindByProcessedUrl(string processedUrl)
            {
                Tracker result = null;

                Data.tracker dbTracker = DbSingleton.trackers.FirstOrDefault(x => x.url == processedUrl);
                if (dbTracker != null)
                {
                    result = DbObjectToTracker(dbTracker);
                }

                return result;
            }

            /// <inheritdoc/>
            public override Tracker FindByUrl(string url)
            {
                return this.FindByProcessedUrl(ProcessUrl(url));
            }

            /// <inheritdoc/>
            public override ReturnCode SimpleCreate(Tracker tracker)
            {
                DbSingleton.trackers.Add(new Data.tracker()
                {
                    id = tracker.id,
                    url = tracker.processedUrl,
                    name = tracker.name,
                    customname = tracker.customName,
                    infolink = tracker.infoLink,
                    lastupdate = tracker.lastUpdate,
                    ratelimit = tracker.rateLimit,
                    message = tracker.message,
                });
                DbSingleton.SaveChanges/*Async*/();

                return ReturnCode.Success;
            }

            /// <inheritdoc/>
            public override Tracker Create(string url, string name, string infoLink)
            {
                lock (IdCounterMutex)
                {
                    Tracker newTracker = new Tracker(++idCounter, ProcessUrl(url), name, infoLink, this);

                    ReturnCode result = this.Create(newTracker);
                    if (result != ReturnCode.Success)
                    {
                        newTracker = null;
                        idCounter--;
                    }

                    return newTracker;
                }
            }

            /// <inheritdoc/>
            public override ReturnCode SimpleRead(Tracker tracker)
            {
                ReturnCode result;

                Data.tracker dbTracker = DbSingleton.trackers.FirstOrDefault(x => x.id == tracker.id);
                if (dbTracker != null)
                {
                    tracker.processedUrl = dbTracker.url;
                    result = ReturnCode.Success;
                }
                else
                {
                    result = ReturnCode.NotFound;
                }

                return result;
            }

            /// <inheritdoc/>
            public override ReturnCode SimpleUpdate(Tracker tracker)
            {
                ReturnCode result;

                Data.tracker dbTracker = DbSingleton.trackers.FirstOrDefault(x => x.id == tracker.id);
                if (dbTracker != null)
                {
                    dbTracker.url = tracker.processedUrl;
                    dbTracker.customname = tracker.customName;
                    dbTracker.infolink = tracker.infoLink;
                    DbSingleton.SaveChanges/*Async*/();
                    result = ReturnCode.Success;
                }
                else
                {
                    result = ReturnCode.NotFound;
                }

                return result;
            }

            /// <inheritdoc/>
            public override ReturnCode SimpleDelete(Tracker tracker)
            {
                ReturnCode result;

                Data.tracker dbTracker = DbSingleton.trackers.FirstOrDefault(x => x.id == tracker.id);
                if (dbTracker != null)
                {
                    DbSingleton.trackers.Remove(dbTracker);
                    DbSingleton.SaveChanges/*Async*/();
                    result = ReturnCode.Success;
                }
                else
                {
                    result = ReturnCode.NotFound;
                }

                return result;
            }

            private Tracker DbObjectToTracker(Data.tracker dbObject)
            {
                return new Tracker(
                    dbObject.id,
                    dbObject.url,
                    dbObject.name,
                    dbObject.customname,
                    dbObject.infolink,
                    dbObject.lastupdate,
                    dbObject.ratelimit,
                    dbObject.message,
                    this);
            }

            public override IEnumerable<Tracker> GetAll()
            {
                return DbSingleton.trackers.ToList().Select(x => DbObjectToTracker(x));
            }

            public override Tracker GetOne(int id)
            {
                Tracker result = null;

                Data.tracker dbTracker = DbSingleton.trackers.FirstOrDefault(x => x.id == id);
                if (dbTracker != null)
                {
                    result = DbObjectToTracker(dbTracker);
                }

                return result;
            }
        }
    }
}

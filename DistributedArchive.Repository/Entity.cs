﻿// <copyright file="Entity.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    /// <summary>
    /// Base class of the Entity objects with a reference to the own IRepositoryBase.
    /// </summary>
    /// <typeparam name="T">The final type of the entity.</typeparam>
    public abstract class Entity<T> : IEntity<T>
        where T : Entity<T>
    {
        protected IRepositoryBase<T> repository;

        /// <inheritdoc/>
        public ReturnCode RepositoryCreate(IRepositoryBase<T> repository)
        {
            ReturnCode result;
            IRepositoryBase<T> currentRepository = this.repository;

            if (currentRepository != null && repository != currentRepository)
            {
                // result ignored
                currentRepository.SimpleDelete((T)this);
            }

            result = repository.SimpleCreate((T)this);
            this.repository = repository;

            return result;
        }

        /// <inheritdoc/>
        public ReturnCode RepositoryRead()
        {
            ReturnCode result;
            IRepositoryBase<T> repository = this.repository;

            if (repository != null)
            {
                result = repository.SimpleRead((T)this);
            }
            else
            {
                result = ReturnCode.NoRepository;
            }

            return result;
        }

        /// <inheritdoc/>
        public ReturnCode RepositoryUpdate()
        {
            ReturnCode result;
            IRepositoryBase<T> repository = this.repository;

            if (repository != null)
            {
                result = repository.SimpleUpdate((T)this);
            }
            else
            {
                result = ReturnCode.NoRepository;
            }

            return result;
        }

        /// <inheritdoc/>
        public ReturnCode RepositoryDelete()
        {
            ReturnCode result;
            IRepositoryBase<T> repository = this.repository;

            if (repository != null)
            {
                result = repository.SimpleDelete((T)this);
                this.repository = null;
            }
            else
            {
                result = ReturnCode.NoRepository;
            }

            return result;
        }
    }
}

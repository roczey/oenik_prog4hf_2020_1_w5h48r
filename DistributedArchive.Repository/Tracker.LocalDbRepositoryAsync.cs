﻿// <copyright file="Tracker.LocalDbRepositoryAsync.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    /// <summary>
    /// LocalDbRepositoryAsync wrapper partial class.
    /// </summary>
    public partial class Tracker
    {
        /// <summary>
        /// Tracker repository using the EntityFramework LocalDb and SaveChangesAsync.
        /// </summary>
        public sealed class LocalDbRepositoryAsync : AbstractRepository
        {
            private static readonly Data.LocalDbEntities DbSingleton = new Data.LocalDbEntities();
            private static readonly object IdCounterMutex = new object();
            private static readonly Semaphore RepositoryMutex = new Semaphore(1, 1);
            private static int idCounter = DbSingleton.trackers.Max(x => x.id);

            /// <inheritdoc/>
            public override Tracker FindByProcessedUrl(string processedUrl)
            {
                Tracker result = null;

                RepositoryMutex.WaitOne();
                Data.tracker dbTracker = DbSingleton.trackers.FirstOrDefault(x => x.url == processedUrl);
                RepositoryMutex.Release();
                if (dbTracker != null)
                {
                    result = DbObjectToTracker(dbTracker);
                }

                return result;
            }

            /// <inheritdoc/>
            public override Tracker FindByUrl(string url)
            {
                return this.FindByProcessedUrl(ProcessUrl(url));
            }

            /// <inheritdoc/>
            public override ReturnCode SimpleCreate(Tracker tracker)
            {
                RepositoryMutex.WaitOne();
                DbSingleton.trackers.Add(new Data.tracker() { id = tracker.id, url = tracker.processedUrl });
                DbSingleton.SaveChangesAsync()
                    .ContinueWith((x) => RepositoryMutex.Release());

                return ReturnCode.Success;
            }

            /// <inheritdoc/>
            public override Tracker Create(string url, string name, string infoLink)
            {
                lock (IdCounterMutex)
                {
                    Tracker newTracker = new Tracker(++idCounter, ProcessUrl(url), name, infoLink, this);

                    ReturnCode result = this.Create(newTracker);
                    if (result != ReturnCode.Success)
                    {
                        newTracker = null;
                        idCounter--;
                    }

                    return newTracker;
                }
            }

            /// <inheritdoc/>
            public override ReturnCode SimpleRead(Tracker tracker)
            {
                ReturnCode result;

                RepositoryMutex.WaitOne();
                Data.tracker dbTracker = DbSingleton.trackers.FirstOrDefault(x => x.id == tracker.id);
                RepositoryMutex.Release();
                if (dbTracker != null)
                {
                    tracker.processedUrl = dbTracker.url;
                    result = ReturnCode.Success;
                }
                else
                {
                    result = ReturnCode.NotFound;
                }

                return result;
            }

            /// <inheritdoc/>
            public override ReturnCode SimpleUpdate(Tracker tracker)
            {
                ReturnCode result;

                RepositoryMutex.WaitOne();
                Data.tracker dbTracker = DbSingleton.trackers.FirstOrDefault(x => x.id == tracker.id);
                if (dbTracker != null)
                {
                    dbTracker.url = tracker.processedUrl;
                    DbSingleton.SaveChangesAsync()
                        .ContinueWith((x) => RepositoryMutex.Release());
                    result = ReturnCode.Success;
                }
                else
                {
                    RepositoryMutex.Release();
                    result = ReturnCode.NotFound;
                }

                return result;
            }

            /// <inheritdoc/>
            public override ReturnCode SimpleDelete(Tracker tracker)
            {
                ReturnCode result;

                RepositoryMutex.WaitOne();
                Data.tracker dbTracker = DbSingleton.trackers.FirstOrDefault(x => x.id == tracker.id);
                if (dbTracker != null)
                {
                    DbSingleton.trackers.Remove(dbTracker);
                    DbSingleton.SaveChangesAsync()
                        .ContinueWith((x) => RepositoryMutex.Release());
                    result = ReturnCode.Success;
                }
                else
                {
                    RepositoryMutex.Release();
                    result = ReturnCode.NotFound;
                }

                return result;
            }

            private Tracker DbObjectToTracker(Data.tracker dbObject)
            {
                return new Tracker(
                    dbObject.id,
                    dbObject.url,
                    dbObject.name,
                    dbObject.customname,
                    dbObject.infolink,
                    dbObject.lastupdate,
                    dbObject.ratelimit,
                    dbObject.message,
                    this);
            }

            public override IEnumerable<Tracker> GetAll()
            {
                return DbSingleton.trackers.Select(x => DbObjectToTracker(x));
            }

            public override Tracker GetOne(int id)
            {
                Tracker result = null;

                Data.tracker dbTracker = DbSingleton.trackers.FirstOrDefault(x => x.id == id);
                if (dbTracker != null)
                {
                    result = DbObjectToTracker(dbTracker);
                }

                return result;
            }
        }
    }
}

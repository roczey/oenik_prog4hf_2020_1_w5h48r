﻿//// <copyright file="Content.LocalDbRepositoryAsync.cs" company="RB">
//// Copyright (c) RB. All rights reserved.
//// </copyright>

//namespace DistributedArchive.Repository
//{
//    using System.Linq;
//    using System.Threading;

//    /// <summary>
//    /// LocalDbRepositoryAsync wrapper partial class.
//    /// </summary>
//    public partial class Content
//    {
//        /// <summary>
//        /// Content repository using the EntityFramework LocalDb and SaveChangesAsync.
//        /// </summary>
//        public sealed class LocalDbRepositoryAsync : AbstractRepository
//        {
//            private static readonly Data.LocalDbEntities DbSingleton = new Data.LocalDbEntities();
//            private static readonly object IdCounterMutex = new object();
//            private static readonly Semaphore RepositoryMutex = new Semaphore(1, 1);
//            private static int idCounter = DbSingleton.searchablecontents.Max(x => x.id);

//            /// <inheritdoc/>
//            public override ReturnCode SimpleCreate(Content content)
//            {
//                RepositoryMutex.WaitOne();
//                DbSingleton.searchablecontents.Add(new Data.searchablecontent() { id = content.id, url = content.processedUrl });
//                DbSingleton.SaveChangesAsync()
//                    .ContinueWith((x) => RepositoryMutex.Release());

//                return ReturnCode.Success;
//            }

//            /// <inheritdoc/>
//            public override Content Create(string url, string name, string infoLink)
//            {
//                lock (IdCounterMutex)
//                {
//                    Content newContent = new Content(++idCounter, ProcessUrl(url), name, infoLink);

//                    ReturnCode result = this.Create(newContent);
//                    if (result != ReturnCode.Success)
//                    {
//                        newContent = null;
//                        idCounter--;
//                    }

//                    return newContent;
//                }
//            }

//            /// <inheritdoc/>
//            public override ReturnCode SimpleRead(Content content)
//            {
//                ReturnCode result;

//                RepositoryMutex.WaitOne();
//                Data.content dbContent = DbSingleton.contents.FirstOrDefault(x => x.id == content.id);
//                RepositoryMutex.Release();
//                if (dbContent != null)
//                {
//                    content.processedUrl = dbContent.url;
//                    result = ReturnCode.Success;
//                }
//                else
//                {
//                    result = ReturnCode.NotFound;
//                }

//                return result;
//            }

//            /// <inheritdoc/>
//            public override ReturnCode SimpleUpdate(Content content)
//            {
//                ReturnCode result;

//                RepositoryMutex.WaitOne();
//                Data.content dbContent = DbSingleton.contents.FirstOrDefault(x => x.id == content.id);
//                if (dbContent != null)
//                {
//                    dbContent.url = content.processedUrl;
//                    DbSingleton.SaveChangesAsync()
//                        .ContinueWith((x) => RepositoryMutex.Release());
//                    result = ReturnCode.Success;
//                }
//                else
//                {
//                    RepositoryMutex.Release();
//                    result = ReturnCode.NotFound;
//                }

//                return result;
//            }

//            /// <inheritdoc/>
//            public override ReturnCode SimpleDelete(Content content)
//            {
//                ReturnCode result;

//                RepositoryMutex.WaitOne();
//                Data.content dbContent = DbSingleton.contents.FirstOrDefault(x => x.id == content.id);
//                if (dbContent != null)
//                {
//                    DbSingleton.contents.Remove(dbContent);
//                    DbSingleton.SaveChangesAsync()
//                        .ContinueWith((x) => RepositoryMutex.Release());
//                    result = ReturnCode.Success;
//                }
//                else
//                {
//                    RepositoryMutex.Release();
//                    result = ReturnCode.NotFound;
//                }

//                return result;
//            }

//            private static Content DbObjectToContent(Data.content dbObject)
//            {
//                return new Content(
//                    dbObject.id,
//                    dbObject.url,
//                    dbObject.name,
//                    dbObject.customname,
//                    dbObject.infolink,
//                    dbObject.lastupdate,
//                    dbObject.ratelimit,
//                    dbObject.message);
//            }
//        }
//    }
//}

﻿// <copyright file="ReturnCode.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    /// <summary>
    /// Simple return codes that describe the result of the operations.
    /// </summary>
    public enum ReturnCode
    {
        /// <summary>
        /// The command was executed successfully.
        /// </summary>
        Success,

        /// <summary>
        /// The record was not found in the repository.
        /// </summary>
        NotFound,

        /// <summary>
        /// There is no repository attached to the entity.
        /// </summary>
        NoRepository,
    }
}

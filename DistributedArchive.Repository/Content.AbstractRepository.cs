﻿// <copyright file="Content.AbstractRepository.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    using System.Text.RegularExpressions;

    /// <summary>
    /// Wrapper partial class for AbstractRepository.
    /// </summary>
    public sealed partial class Content
    {
        /// <summary>
        /// Base class for the Content repository implementations.
        /// </summary>
        public abstract class AbstractRepository : Repository<Content>
        {
            /*/// <summary>
            /// Find contents in the repository by its name.
            /// </summary>
            /// <param name="name">The name of the content to find.</param>
            /// <returns>An array of the matched contents.</returns>
            public abstract Content[] FindByName(string name);

            /// <summary>
            /// Find contents in the repository by its name.
            /// </summary>
            /// <param name="pattern">The pattern to match the content name against.</param>
            /// <returns>An array of the matched contents.</returns>
            public abstract Content[] FindByName(Regex pattern);*/

            /// <summary>
            /// Creates a new Content object inside the repository.
            /// </summary>
            /// <param name="type">The media type of the content.</param>
            /// <param name="flags">Binary flags associated with the content.</param>
            /// <param name="title">Original title of the content.</param>
            /// <param name="path">Path of the content file.</param>
            /// <param name="addedTime">The time when the content was added to the local client.</param>
            /// <returns>A new Content object.</returns>
            public abstract Content Create(ContentType type, ContentFlags flags, string title, string path, long addedTime);
        }
    }
}

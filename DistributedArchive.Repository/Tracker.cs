﻿// <copyright file="Tracker.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    /// <summary>
    /// Entity class for the trackers.
    /// </summary>
    public sealed partial class Tracker : Entity<Tracker>
    {
        private readonly int id;
        private string processedUrl;
        private string name;
        private string customName;
        private string infoLink;
        private long lastUpdate;
        private int rateLimit;
        private string message;

        private Tracker(int id, string processedUrl, string name, string customName, string infoLink, long lastUpdate, int rateLimit, string message, AbstractRepository repository)
        {
            this.id = id;
            this.processedUrl = processedUrl;
            this.name = name;
            this.customName = customName;
            this.infoLink = infoLink;
            this.lastUpdate = lastUpdate;
            this.rateLimit = rateLimit;
            this.message = message;
            this.repository = repository;
        }

        private Tracker(int id, string processedUrl, string name, string infoLink, AbstractRepository repository)
        {
            this.id = id;
            this.processedUrl = processedUrl;
            this.name = name;
            this.infoLink = infoLink;
            this.repository = repository;
        }

        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets the lowercase, trailing slash removed url.
        /// </summary>
        public string ProcessedUrl
        {
            get { return this.processedUrl; }
            set { this.processedUrl = value; }
        }

        /// <summary>
        /// Gets or sets the name of the tracker, custom name if available.
        /// </summary>
        public string Name
        {
            get { return this.customName ?? this.name; }
            set { this.customName = value; }
        }

        /// <summary>
        /// Gets the name of the tracker.
        /// </summary>
        public string RealName
        {
            get { return this.name; }
        }

        /// <summary>
        /// Gets or sets the custom (user defined) name of the tracker.
        /// </summary>
        public string CustomName
        {
            get { return this.customName; }
            set { this.customName = value; }
        }

        /// <summary>
        /// Gets the link to the website of the tracker.
        /// </summary>
        public string InfoLink
        {
            get { return this.infoLink; }
            set { this.infoLink = value; }
        }

        /// <summary>
        /// Turns the url lowercase and removes the trailing slash.
        /// </summary>
        /// <param name="url">The url to process.</param>
        /// <returns>A processed url string.</returns>
        public static string ProcessUrl(string url)
        {
            // TODO: url escapes?
            return url.Replace('\\', '/').TrimEnd('/').ToLowerInvariant();
        }
    }
}

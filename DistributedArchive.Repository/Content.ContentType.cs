﻿// <copyright file="Content.ContentType.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    /// <summary>
    /// Wrapper partial class for ContentType enum.
    /// </summary>
    public sealed partial class Content
    {
        /// <summary>
        /// The (media) type of the content file.
        /// </summary>
        public enum ContentType : byte
        {
            /// <summary>
            /// Grouped content, possibly compressed archive file.
            /// </summary>
            Group = 0,

            /// <summary>
            /// A raster image file.
            /// </summary>
            Image = 1,

            /// <summary>
            /// A video file.
            /// </summary>
            Video = 2,

            /// <summary>
            /// A text file.
            /// </summary>
            Text = 3,
        }
    }
}

﻿// <copyright file="Repository.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Base class of the repository, has default implementation for IRepository.
    /// </summary>
    /// <typeparam name="T">IEntity, class of the entities stored in this repository.</typeparam>
    public abstract class Repository<T> : IRepositoryBase<T>, IRepository<T>
        where T : IEntity<T>
    {
        /// <summary>
        /// Adds the entity to the repository using the entity's own method.
        /// </summary>
        /// <param name="entity">The entity to add to the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        public ReturnCode Create(T entity)
        {
            return entity.RepositoryCreate(this);
        }

        /// <summary>
        /// Reads the entity from the repository using the entity's own method.
        /// </summary>
        /// <param name="entity">The entity to read from the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        public ReturnCode Read(T entity)
        {
            return entity.RepositoryRead();
        }

        /// <summary>
        /// Updates the entity in the repository using the entity's own method.
        /// </summary>
        /// <param name="entity">The entity to update in the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        public ReturnCode Update(T entity)
        {
            return entity.RepositoryUpdate();
        }

        /// <summary>
        /// Deletes the entity from the repository using the entity's own method.
        /// </summary>
        /// <param name="entity">The entity to delete from the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        public ReturnCode Delete(T entity)
        {
            return entity.RepositoryDelete();
        }

        /// <summary>
        /// Adds the entity to the repository without caring for the entity's repository reference.
        /// </summary>
        /// <param name="entity">The entity to add to the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        public abstract ReturnCode SimpleCreate(T entity);

        /// <summary>
        /// Reads the entity from the repository without caring for the entity's repository reference.
        /// </summary>
        /// <param name="entity">The entity to read from the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        public abstract ReturnCode SimpleRead(T entity);

        /// <summary>
        /// Updates the entity in the repository without caring for the entity's repository reference.
        /// </summary>
        /// <param name="entity">The entity to update in the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        public abstract ReturnCode SimpleUpdate(T entity);

        /// <summary>
        /// Deletes the entity from the repository without caring for the entity's repository reference.
        /// </summary>
        /// <param name="entity">The entity to delete from the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        public abstract ReturnCode SimpleDelete(T entity);
    }
}

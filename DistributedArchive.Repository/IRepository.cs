﻿// <copyright file="IRepository.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    /// <summary>
    /// Interface that hides IRepositoryBase in order to add seamless support for entities that have a repository reference.
    /// </summary>
    /// <typeparam name="T">IEntity, class of the entities stored in this repository.</typeparam>
    public interface IRepository<T>
        where T : IEntity<T>
    {
        /// <summary>
        /// Adds the entity to the repository using the entity's own method.
        /// </summary>
        /// <param name="entity">The entity to add to the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode Create(T entity);

        /// <summary>
        /// Reads the entity from the repository using the entity's own method.
        /// </summary>
        /// <param name="entity">The entity to read from the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode Read(T entity);

        /// <summary>
        /// Updates the entity in the repository using the entity's own method.
        /// </summary>
        /// <param name="entity">The entity to update in the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode Update(T entity);

        /// <summary>
        /// Deletes the entity from the repository using the entity's own method.
        /// </summary>
        /// <param name="entity">The entity to delete from the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode Delete(T entity);
    }
}

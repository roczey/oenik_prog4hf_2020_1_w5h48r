﻿// <copyright file="Content.ContentFlags.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    using System;

    /// <summary>
    /// Wrapper partial class for ContentFlags.
    /// </summary>
    public sealed partial class Content
    {
        /// <summary>
        /// Binary flags that help describe a content object.
        /// </summary>
        [Flags]
        public enum ContentFlags : byte
        {
            /// <summary>
            /// Placeholder when no flags are set.
            /// </summary>
            None = 0,
        }
    }
}

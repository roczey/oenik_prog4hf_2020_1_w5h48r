﻿// <copyright file="IRepositoryBase.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    /// <summary>
    /// Base interface of the repository for simple implementation.
    /// </summary>
    /// <typeparam name="T">IEntity, class of the entities stored in this repository.</typeparam>
    public interface IRepositoryBase<T>
        where T : IEntity<T>
    {
        /// <summary>
        /// Adds the entity to the repository.
        /// </summary>
        /// <param name="entity">The entity to add to the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode SimpleCreate(T entity);

        /// <summary>
        /// Reads the entity from the repository.
        /// </summary>
        /// <param name="entity">The entity to read from the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode SimpleRead(T entity);

        /// <summary>
        /// Updates the entity in the repository.
        /// </summary>
        /// <param name="entity">The entity to update in the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode SimpleUpdate(T entity);

        /// <summary>
        /// Deletes the entity from the repository.
        /// </summary>
        /// <param name="entity">The entity to delete from the repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode SimpleDelete(T entity);
    }
}

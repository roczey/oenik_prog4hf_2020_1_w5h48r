﻿// <copyright file="IEntity.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    /// <summary>
    /// Interface of the Entity objects.
    /// </summary>
    /// <typeparam name="T">The final type of the entity.</typeparam>
    public interface IEntity<T>
        where T : IEntity<T>
    {
        /// <summary>
        /// Adds this entity to its repository.
        /// </summary>
        /// <param name="repository">The containing repository.</param>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode RepositoryCreate(IRepositoryBase<T> repository);

        /// <summary>
        /// Reads this entity from its repository.
        /// </summary>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode RepositoryRead();

        /// <summary>
        /// Updates this entity from its repository.
        /// </summary>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode RepositoryUpdate();

        /// <summary>
        /// Deletes this entity from its repository.
        /// </summary>
        /// <returns>ReturnCode, implementation specific.</returns>
        ReturnCode RepositoryDelete();
    }
}

﻿// <copyright file="Tracker.AbstractRepository.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

using System.Collections;
using System.Collections.Generic;

namespace DistributedArchive.Repository
{
    /// <summary>
    /// Wrapper for AbstractRepository.
    /// </summary>
    public partial class Tracker
    {
        /// <summary>
        /// Base class for the Tracker repository implementations.
        /// </summary>
        public abstract class AbstractRepository : Repository<Tracker>
        {
            /// <summary>
            /// Find tracker in the repository by its url.
            /// </summary>
            /// <param name="url">The url of the tracker to find.</param>
            /// <returns>The tracker if it exists or null.</returns>
            public abstract Tracker FindByUrl(string url);

            /// <summary>
            /// Find tracker in the repository by its processed url.
            /// </summary>
            /// <param name="processedUrl">The processed url of the tracker to find.</param>
            /// <returns>The tracker if it exists or null.</returns>
            public abstract Tracker FindByProcessedUrl(string processedUrl);

            /// <summary>
            /// Creates a new Tracker object inside the repository.
            /// </summary>
            /// <param name="url">The url of the tracker.</param>
            /// <param name="name">The name of the tracker.</param>
            /// <param name="infoUrl">The url for the website of the tracker.</param>
            /// <returns>A new Tracker object.</returns>
            public abstract Tracker Create(string url, string name, string infoUrl);

            public abstract IEnumerable<Tracker> GetAll();
            public abstract Tracker GetOne(int id);
        }
    }
}

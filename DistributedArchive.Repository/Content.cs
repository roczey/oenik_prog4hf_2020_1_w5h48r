﻿// <copyright file="Content.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Repository
{
    /// <summary>
    /// Entity class for the content.
    /// </summary>
    public sealed partial class Content : Entity<Content>
    {
        private readonly int id;
        private byte type;
        private byte flags;
        private long timestamp;
        private uint thumbnailDimensions;
        private string title;
        private string customTitle;
        private string path;
        private long addedTime;

        private Content(int id, byte type, byte flags, long timestamp, uint thumbnailDimensions, string title, string customTitle, string path, long addedTime)
        {
            this.id = id;
            this.type = type;
            this.flags = flags;
            this.timestamp = timestamp;
            this.thumbnailDimensions = thumbnailDimensions;
            this.title = title;
            this.customTitle = customTitle;
            this.path = path;
            this.addedTime = addedTime;
        }

        private Content(int id, byte type, byte flags, string title, string path, long addedTime)
        {
            this.id = id;
            this.type = type;
            this.flags = flags;
            this.title = title;
            this.path = path;
            this.addedTime = addedTime;
        }

        /// <summary>
        /// Gets the type of the content.
        /// </summary>
        public ContentType Type
        {
            get { return (ContentType)this.type; }
        }
    }
}

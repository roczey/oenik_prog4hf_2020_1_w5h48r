﻿// <copyright file="Program.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The main class of the console app.
    /// </summary>
    internal class Program
    {
        private static void Main(/*string[] args*/) // TODO: command line switches
        {
            while (true)
            {
                Console.Write(
@"1: List added contents
2: Add content by hash and tracker
3: Remove content
4: Change content title
5: Show tracker statistics
6: Debug add content record
7: Debug update content record
8: Debug add content hash
9: Debug remove content hash
10: Show content hashes
11: Find content by title
Selected menu option: ");

                string selection = Console.ReadLine();

                switch (selection)
                {
                    default:
                        Console.WriteLine("Ez még nincs kész");
                        break;
                }

                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}

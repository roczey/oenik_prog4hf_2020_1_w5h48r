﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DistributedArchive.Wpf
{
	class MainVM : ViewModelBase
	{
		private MainLogic logic;
		private ObservableCollection<TrackerVM> trackers;
		private TrackerVM selectedTracker;

		public TrackerVM SelectedTracker
		{
			get { return selectedTracker; }
			set { Set(ref selectedTracker, value); }
		}
		public ObservableCollection<TrackerVM> AllTrackers
		{
			get { return trackers; }
			set { Set(ref trackers, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<TrackerVM, bool> EditorFunc { get; set; }

		public MainVM()
		{
			logic = new MainLogic();

			LoadCmd = new RelayCommand(() =>
					AllTrackers = new ObservableCollection<TrackerVM>(logic.ApiGetTrackers()));
			DelCmd = new RelayCommand(() => logic.ApiDelTracker(selectedTracker));
			AddCmd = new RelayCommand(() => logic.EditTracker(null, EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditTracker(selectedTracker, EditorFunc));
		}
	}
}

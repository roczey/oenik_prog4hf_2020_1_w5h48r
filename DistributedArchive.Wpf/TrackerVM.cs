﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedArchive.Wpf
{
    class TrackerVM : ObservableObject
    {
        private int id;
        private string url;
        private string name;
        private string infoUrl;

        public int Id
        {
            get { return id; }
            set { Set(ref id, value); }
        }
        public string Url
        {
            get { return url; }
            set { Set(ref url, value); }
        }
        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }
        public string InfoUrl
        {
            get { return infoUrl; }
            set { Set(ref infoUrl, value); }
        }

        public void CopyFrom(TrackerVM other)
        {
            if (other == null) return;
            this.id = other.id;
            this.url = other.url;
            this.name = other.name;
            this.infoUrl = other.infoUrl;
        }
    }
}

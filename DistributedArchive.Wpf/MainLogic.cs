﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DistributedArchive.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:8644/api/TrackersApi/";
        HttpClient client = new HttpClient();

        void SendResponse(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "TrackerResult");
        }

        public List<TrackerVM> ApiGetTrackers()
        {
            string json = client.GetStringAsync(url + "all").Result;
            List<TrackerVM> trackers = JsonConvert.DeserializeObject<List<TrackerVM>>(json);
            return trackers;
        }

        public void ApiDelTracker(TrackerVM Tracker)
        {
            bool success = false;
            if (Tracker != null)
            {
                string json = client.GetStringAsync(url + "del/" + Tracker.Id.ToString()).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["SUCC"];
            }
            SendResponse(success);
        }
        bool ApiEditTracker(TrackerVM Tracker, bool isEditing)
        {
            if (Tracker == null) return false;
            string myUrl = url + (isEditing ? "mod" : "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(TrackerVM.Id), Tracker.Id.ToString());
            postData.Add(nameof(TrackerVM.Url), Tracker.Url);
            postData.Add(nameof(TrackerVM.Name), Tracker.Name);
            postData.Add(nameof(TrackerVM.InfoUrl), Tracker.InfoUrl.ToString());
            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["SUCC"];
        }

        public void EditTracker(TrackerVM Tracker, Func<TrackerVM, bool> editor)
        {
            TrackerVM clone = new TrackerVM();
            if (Tracker != null) clone.CopyFrom(Tracker);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (Tracker != null) success = ApiEditTracker(clone, true);
                else success = ApiEditTracker(clone, false);
            }
            SendResponse(success == true);
        }
    }
}

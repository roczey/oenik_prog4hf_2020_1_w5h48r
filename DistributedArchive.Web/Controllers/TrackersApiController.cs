﻿using AutoMapper;
using DistributedArchive.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DistributedArchive.Web.Controllers
{
    public class TrackersApiController : ApiController
    {
        public class ApiResult
        {
            public bool SUCC;
            public ApiResult(bool success)
            {
                SUCC = success;
            }
        }

        IMapper mapper;
        Repository.Tracker.AbstractRepository repository;
        public TrackersApiController()
        {
            mapper = MapperFactory.CreateMapper();

            repository = new Repository.Tracker.LocalDbRepository();
        }

        // GET api/TrackersApi/all
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Tracker> GetAll()
        {
            var trackers = repository.GetAll();
            return mapper.Map<IEnumerable<Repository.Tracker>, List<Models.Tracker>>(trackers);
        }

        // GET api/TrackersApi/del/5
        [ActionName("del")]
        [HttpGet]
        public ApiResult DeleteTracker(int id)
        {
            bool success = repository.Delete(repository.GetOne(id)) == Repository.ReturnCode.Success;
            return new ApiResult(success);
        }

        // POST api/TrackersApi/add
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddTracker(Tracker tracker)
        {
            bool success = repository.Create(tracker.Url, tracker.Name, tracker.InfoUrl) != null;
            return new ApiResult(success);
        }

        // POST api/TrackersApi/mod
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModTracker(Tracker tracker)
        {
            Repository.Tracker t = repository.GetOne(tracker.Id);
            t.ProcessedUrl = tracker.Url;
            t.Name = tracker.Name;
            t.InfoLink = tracker.InfoUrl;
            bool success = t.RepositoryUpdate() == Repository.ReturnCode.Success;
            return new ApiResult(success);
        }
    }
}

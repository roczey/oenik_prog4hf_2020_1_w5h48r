﻿using AutoMapper;
using DistributedArchive.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DistributedArchive.Web.Controllers
{
    public class TrackersController : Controller
    {
        Repository.Tracker.AbstractRepository repository;
        IMapper mapper;
        TrackersViewModel vm;

        public TrackersController()
        {
            mapper = MapperFactory.CreateMapper();

            repository = new Repository.Tracker.LocalDbRepository();

            vm = new TrackersViewModel();
            vm.EditedTracker = new Tracker();
            var trackers = repository.GetAll();
            vm.ListOfTrackers = mapper.Map<IEnumerable<Repository.Tracker>, List<Models.Tracker>>(trackers);
        }

        private Tracker GetTrackerModel(int id)
        {
            Repository.Tracker tracker = repository.GetOne(id);
            return mapper.Map<Repository.Tracker, Models.Tracker>(tracker);
        }

        // GET: Trackers
        public ActionResult Index()
        {
            ViewData["formAction"] = "AddNew";
            return View("TrackersIndex", vm);
        }

        // GET: Trackers/Details/5
        public ActionResult Details(int id)
        {
            return View("TrackerData", GetTrackerModel(id));
        }

        public ActionResult Remove(int id)
        {
            bool success = repository.Delete(repository.GetOne(id)) == Repository.ReturnCode.Success;
            TempData["formResult"] = success ? "Delete OK" : "Delete FAIL";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ViewData["formAction"] = "Edit";
            vm.EditedTracker = GetTrackerModel(id);
            return View("TrackersIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Tracker tracker, string formAction)
        {
            if(ModelState.IsValid && tracker != null)
            {
                TempData["formResult"] = "Edit OK";
                if(formAction == "AddNew")
                {
                    repository.Create(tracker.Url, tracker.Name, tracker.InfoUrl);
                }
                else
                {
                    var repoTracker = repository.GetOne(tracker.Id);
                    repoTracker.ProcessedUrl = tracker.Url;
                    repoTracker.Name = tracker.Name;
                    repoTracker.InfoLink = tracker.InfoUrl;
                    bool success = repository.Update(repoTracker) == Repository.ReturnCode.Success;
                    if(!success) TempData["formResult"] = "Edit FAIL";
                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["formAction"] = "Edit";
                vm.EditedTracker = tracker;
                return View("TrackersIndex", vm);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DistributedArchive.Web.Models
{
    public class TrackersViewModel
    {
        public Tracker EditedTracker { get; set; }
        public List<Tracker> ListOfTrackers { get; set; }
    }
}
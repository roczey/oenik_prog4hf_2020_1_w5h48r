﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;

namespace DistributedArchive.Web.Models
{
    public class Tracker
    {
        [Display(Name = "Tracker ID")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Tracker URL")]
        [Required]
        public string Url { get; set; }

        [Display(Name = "Tracker Name")]
        public string Name { get; set; }

        [Display(Name = "Tracker Info URL")]
        [RegexStringValidator("^https://")]
        public string InfoUrl { get; set; }
    }
}
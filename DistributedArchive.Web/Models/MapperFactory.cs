﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace DistributedArchive.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration((conf) =>
            {
                conf.CreateMap<DistributedArchive.Repository.Tracker, DistributedArchive.Web.Models.Tracker>()
                    .ForMember(dst => dst.Id, map => map.MapFrom(src => src.Id))
                    .ForMember(dst => dst.Url, map => map.MapFrom(src => src.ProcessedUrl))
                    .ForMember(dst => dst.Name, map => map.MapFrom(src => src.Name))
                    .ForMember(dst => dst.InfoUrl, map => map.MapFrom(src => src.InfoLink));
            });

            return config.CreateMapper();
        }
    }
}
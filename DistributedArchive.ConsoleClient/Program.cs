﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DistributedArchive.ConsoleClient
{
    public class Tracker
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string InfoUrl { get; set; }
        public override string ToString()
        {
            return $"ID={Id}\tName={Name}\tURL={Url}\tInfoUrl={InfoUrl}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:8644/api/TrackersApi/";
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Tracker>>(json);
                foreach (var item in list) Console.WriteLine(item);
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Tracker.Url), "https://cool.new.url");
                postData.Add(nameof(Tracker.Name), "Best Site ever");
                postData.Add(nameof(Tracker.InfoUrl), "https://some.git.readme");

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int trackerId = JsonConvert.DeserializeObject<List<Tracker>>(json).Single(x => x.Name == "Best Site ever").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Tracker.Id), trackerId.ToString());
                postData.Add(nameof(Tracker.Url), "https://cool.new.url");
                postData.Add(nameof(Tracker.Name), "Best Site in my city");
                postData.Add(nameof(Tracker.InfoUrl), "https://some.git.readme");

                response = client.PostAsync(url + "mod",
                    new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "all").Result);
                Console.ReadLine();

                Console.WriteLine("DEL: " + client.GetStringAsync(url + "del/" + trackerId).Result);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "all").Result);
                Console.ReadLine();
            }
        }
    }
}
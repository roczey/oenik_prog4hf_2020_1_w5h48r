﻿//// <copyright file="Content.cs" company="RB">
//// Copyright (c) RB. All rights reserved.
//// </copyright>

//namespace DistributedArchive.Logic
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Text;
//    using System.Threading.Tasks;

//    using AbstractRepository = DistributedArchive.Repository.Content.AbstractRepository;
//    using ReturnCode = DistributedArchive.Repository.ReturnCode;

//    /// <summary>
//    /// Content object logic.
//    /// </summary>
//    public partial class Content
//    {
//        private static AbstractRepository repositorySingleton;
//        private readonly Repository.Content content;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="Content"/> class.
//        /// </summary>
//        public Content()
//        {
//            this.content = RepositorySingleton.Create(url, NameGeneratorSingleton.Generate(url), null);
//        }

//        private Content(Repository.Content content)
//        {
//            this.content = content;
//        }

//        private static AbstractRepository RepositorySingleton
//        {
//            get
//            {
//                return repositorySingleton ??
//                    (repositorySingleton = new Repository.Content.LocalDbRepositoryAsync());
//            }

//            set
//            {
//                repositorySingleton = value;
//            }
//        }

//        /// <summary>
//        /// Reads the entity from the repository again.
//        /// </summary>
//        /// <returns>true on success.</returns>
//        public bool Reload()
//        {
//            ReturnCode result = this.content.RepositoryRead();
//            return result == ReturnCode.Success;
//        }

//        /// <summary>
//        /// Writes the entity to the repository.
//        /// </summary>
//        /// <returns>true on success.</returns>
//        public bool Save()
//        {
//            ReturnCode result = this.content.RepositoryUpdate();
//            return result == ReturnCode.Success;
//        }

//        /// <summary>
//        /// Removes the entity from the repository.
//        /// </summary>
//        /// <returns>true on success.</returns>
//        public bool Delete()
//        {
//            ReturnCode result = this.content.RepositoryDelete();
//            return result == ReturnCode.Success;
//        }
//    }
//}

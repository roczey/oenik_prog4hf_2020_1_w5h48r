﻿// <copyright file="Tracker.CapsTrimNameGenerator.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Logic
{
    using System.Text.RegularExpressions;

    /// <summary>
    /// Wrapper partial class for CapsTrimNameGenerator.
    /// </summary>
    public partial class Tracker
    {
        /// <summary>
        /// Generates a tracker name with some trimming and all caps.
        /// </summary>
        public class CapsTrimNameGenerator : INameGenerator
        {
            /// <inheritdoc/>
            public string Generate(string url)
            {
                string result;

                Match importantParts = Regex.Match(url, @"^([\w-]+:\/\/)?(?:((?:[\w-]+\.)+(?:[\w-]+))\.([A-Za-z][\w-]*)|([\w.\[:\]]+))");
                if (!importantParts.Success)
                {
                    result = url;
                }
                else
                {
                    // Capture protocol = importantParts.Groups[1];
                    // Capture tld = importantParts.Groups[3];
                    Capture domainWithoutTld = importantParts.Groups[2];
                    Capture ipOrLocalhost = importantParts.Groups[4];
                    result = (domainWithoutTld?.Value ?? ipOrLocalhost?.Value).ToUpperInvariant();
                }

                return result;
            }
        }
    }
}

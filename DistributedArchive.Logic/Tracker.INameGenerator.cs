﻿// <copyright file="Tracker.INameGenerator.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Logic
{
    /// <summary>
    /// Wrapper partial class for INameGenerator.
    /// </summary>
    public partial class Tracker
    {
        /// <summary>
        /// Interface to generate default names for trackers.
        /// </summary>
        public interface INameGenerator
        {
            /// <summary>
            /// Generates a default tracker name based on the URL.
            /// </summary>
            /// <param name="url">The url of the tracker.</param>
            /// <returns>A string that represents a vague name for the tracker.</returns>
            string Generate(string url);
        }
    }
}

﻿// <copyright file="Tracker.cs" company="RB">
// Copyright (c) RB. All rights reserved.
// </copyright>

namespace DistributedArchive.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using AbstractRepository = DistributedArchive.Repository.Tracker.AbstractRepository;
    using ReturnCode = DistributedArchive.Repository.ReturnCode;

    /// <summary>
    /// Tracker object logic.
    /// </summary>
    public partial class Tracker
    {
        private static AbstractRepository repositorySingleton;
        private static INameGenerator nameGeneratorSignleton;
        private readonly Repository.Tracker tracker;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tracker"/> class.
        /// </summary>
        /// <param name="url">Url of the tracker.</param>
        public Tracker(string url)
        {
            this.tracker = RepositorySingleton.Create(url, NameGeneratorSingleton.Generate(url), null);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Tracker"/> class.
        /// </summary>
        /// <param name="url">Url of the tracker.</param>
        /// <param name="name">Name of the tracker.</param>
        /// <param name="infoUrl">Website of the tracker.</param>
        public Tracker(string url, string name, string infoUrl)
        {
            this.tracker = RepositorySingleton.Create(url, name, infoUrl);
        }

        private Tracker(Repository.Tracker tracker)
        {
            this.tracker = tracker;
        }

        /// <summary>
        /// Gets the url of the tracker.
        /// </summary>
        public string Url
        {
            get { return this.tracker.ProcessedUrl; }
        }

        private static AbstractRepository RepositorySingleton
        {
            get
            {
                return repositorySingleton ??
                    (repositorySingleton = new Repository.Tracker.LocalDbRepositoryAsync());
            }

            set
            {
                repositorySingleton = value;
            }
        }

        private static INameGenerator NameGeneratorSingleton
        {
            get
            {
                return nameGeneratorSignleton ??
                    (nameGeneratorSignleton = new CapsTrimNameGenerator());
            }

            set
            {
                nameGeneratorSignleton = value;
            }
        }

        /// <summary>
        /// Attempts to find the tracker in the repository by url.
        /// </summary>
        /// <param name="url">Url of the tracker.</param>
        /// <returns>A Tracker object or null.</returns>
        public static Tracker FindByUrl(string url)
        {
            Tracker result = null;

            Repository.Tracker repositoryTracker = RepositorySingleton.FindByUrl(url);
            if (repositoryTracker != null)
            {
                result = new Tracker(repositoryTracker);
            }

            return result;
        }

        /// <summary>
        /// Reads the entity from the repository again.
        /// </summary>
        /// <returns>true on success.</returns>
        public bool Reload()
        {
            ReturnCode result = this.tracker.RepositoryRead();
            return result == ReturnCode.Success;
        }

        /// <summary>
        /// Writes the entity to the repository.
        /// </summary>
        /// <returns>true on success.</returns>
        public bool Save()
        {
            ReturnCode result = this.tracker.RepositoryUpdate();
            return result == ReturnCode.Success;
        }

        /// <summary>
        /// Removes the entity from the repository.
        /// </summary>
        /// <returns>true on success.</returns>
        public bool Delete()
        {
            ReturnCode result = this.tracker.RepositoryDelete();
            return result == ReturnCode.Success;
        }

        public static IEnumerable<Tracker> GetAll()
        {
            return RepositorySingleton.GetAll().Select(x => new Tracker(x));
        }
    }
}

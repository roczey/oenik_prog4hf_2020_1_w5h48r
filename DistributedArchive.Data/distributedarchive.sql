﻿CREATE TABLE [dbo].[trackers] (
    [id]  INT           IDENTITY (1, 1) NOT NULL,
    [url] VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_trackers_url]
    ON [dbo].[trackers]([url] ASC);


SET IDENTITY_INSERT [dbo].[trackers] ON
INSERT INTO [dbo].[trackers] ([id], [url]) VALUES (1, N'http://localhost')
SET IDENTITY_INSERT [dbo].[trackers] OFF



CREATE TABLE [dbo].[searchablecontent] (
    [id]                  INT           IDENTITY (1, 1) NOT NULL,
    [type]                TINYINT       NOT NULL,
    [flags]               TINYINT       NOT NULL,
    [timestamp]           BIGINT        NOT NULL,
    [thumbnail]           VARCHAR (255) DEFAULT (NULL) NULL,
    [thumbnaildimensions] INT           NOT NULL,
    [title]               VARCHAR (255) DEFAULT (NULL) NULL,
    [customtitle]         VARCHAR (255) DEFAULT (NULL) NULL,
    [path]                VARCHAR (255) DEFAULT (NULL) NULL,
    [addedtime]           BIGINT        NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


SET IDENTITY_INSERT [dbo].[searchablecontent] ON
INSERT INTO [dbo].[searchablecontent] ([id], [type], [flags], [timestamp], [thumbnail], [thumbnaildimensions], [title], [customtitle], [path], [addedtime]) VALUES (2, 1, 0, 1572030750670, NULL, 0, N'Cool image', NULL, NULL, 1572030750670)
INSERT INTO [dbo].[searchablecontent] ([id], [type], [flags], [timestamp], [thumbnail], [thumbnaildimensions], [title], [customtitle], [path], [addedtime]) VALUES (3, 2, 0, 1572030852946, NULL, 0, N'Great videeo', NULL, NULL, 1572030852946)
INSERT INTO [dbo].[searchablecontent] ([id], [type], [flags], [timestamp], [thumbnail], [thumbnaildimensions], [title], [customtitle], [path], [addedtime]) VALUES (4, 0, 0, 1572030897469, NULL, 0, N'A zip file?', NULL, NULL, 1572030897469)
SET IDENTITY_INSERT [dbo].[searchablecontent] OFF


CREATE TABLE [dbo].[contenttrackers] (
    [contentid] INT NOT NULL,
    [trackerid] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([contentid] ASC, [trackerid] ASC)
);

INSERT INTO [dbo].[contenttrackers] ([contentid], [trackerid]) VALUES (2, 1)
INSERT INTO [dbo].[contenttrackers] ([contentid], [trackerid]) VALUES (3, 1)
INSERT INTO [dbo].[contenttrackers] ([contentid], [trackerid]) VALUES (4, 1)



CREATE TABLE [dbo].[contenthashes] (
    [contentid] INT             NOT NULL,
    [type]      TINYINT         NOT NULL,
    [hash]      VARBINARY (MAX) NOT NULL,
    PRIMARY KEY CLUSTERED ([contentid] ASC, [type] ASC)
);



INSERT INTO [dbo].[contenthashes] ([contentid], [type], [hash]) VALUES (2, 1, 0x0201)
INSERT INTO [dbo].[contenthashes] ([contentid], [type], [hash]) VALUES (2, 2, 0x0202)
INSERT INTO [dbo].[contenthashes] ([contentid], [type], [hash]) VALUES (3, 2, 0x0302)
INSERT INTO [dbo].[contenthashes] ([contentid], [type], [hash]) VALUES (4, 3, 0x0403)